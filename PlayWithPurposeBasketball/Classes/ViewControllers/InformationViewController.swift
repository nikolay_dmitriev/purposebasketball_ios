//
// Created by Nikolay Dmitriev on 7/2/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class InformationViewController: UIViewController{

//Mark:-Outlets
    @IBOutlet weak var informationTextView: UITextView!


//Mark:-Variables

    var informationText = ""

//Mark:-Lifecyce

    override func viewDidLoad() {
        super.viewDidLoad()
        informationTextView.text = informationText
    }


}