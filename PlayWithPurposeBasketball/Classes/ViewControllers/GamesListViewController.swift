//
// Created by mac-184 on 7/6/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class GamesListViewController: UIViewController {

//Mark:-Outlets

    @IBOutlet var tableView: UITableView!

//Mark:-Variables

    let infromationText = "Click on the Play with Purpose logo at the top of the screen to learn more about our program or to make a donation.\n" +
            "\n" +
            "Click the red circle at the bottom of the screen to add a new game.\n" +
            "\n" +
            "Click 'Change Photo' to take a picture or select an existing one. Enter the player name, number, and tournament name. Click the check on the upper right of the screen to save the game.\n" +
            "\n" +
            "To edit a game, press and hold - to delete a game, swipe to the left."

//Mark:-Lifestyle

    var games: [GameModel] = []
    private var selectedGame: GameModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false

        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(GamesListViewController.handleTap(_:)))
        tableView.userInteractionEnabled = true
        tableView.addGestureRecognizer(gestureRecognizer)

        let longGestureRecongizer = UILongPressGestureRecognizer(target: self, action: #selector(GamesListViewController.handleLongTap(_:)))
        longGestureRecongizer.minimumPressDuration = 1.0
        tableView.addGestureRecognizer(longGestureRecongizer)
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        loadGames()
        selectedGame = nil

    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if segue.identifier == Constants.createEditGameSegue {
            (segue.destinationViewController as! CreateGameViewController).initWithGame(selectedGame)
        }

        if segue.identifier == Constants.extendedGameSegue {
            (segue.destinationViewController as! GameViewController).currentGame = selectedGame
        }

        if segue.identifier == Constants.informationSegue {
            (segue.destinationViewController as! InformationViewController).informationText = infromationText
        }
    }


    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        if let index = tableView.indexPathForRowAtPoint(gestureRecognizer.locationInView(tableView)) {
            selectedGame = games[index.row]
            performSegueWithIdentifier(Constants.extendedGameSegue, sender: self)
        }
    }

    func handleLongTap(gestureRecognizer: UIGestureRecognizer) {
        if gestureRecognizer.state != UIGestureRecognizerState.Began {
            return
        }
        if let index = tableView.indexPathForRowAtPoint(gestureRecognizer.locationInView(tableView)) {
            selectedGame = games[index.row]
            performSegueWithIdentifier(Constants.createEditGameSegue, sender: self)
        }
    }

//Mark:-Actions

    @IBAction func onLogoTap(sender: AnyObject) {
        let url = "http://www.playwithpurposesports.org/"
        UIApplication.sharedApplication().openURL(NSURL(string: url)!)
    }

//Mark:-Private

    private func loadGames() {
        let realm = try! Realm()
        games = realm.objects(GameModel.self).sort({
            $0.date.compare($1.date) == NSComparisonResult.OrderedDescending
        })

        tableView.reloadData()
    }

//Mark:-Constants

    struct Constants {
        static let cellId = "gameCellId"
        static let createEditGameSegue = "createEditGame"
        static let extendedGameSegue = "extendedGameSegue"
        static let informationSegue = "gameListInfromationSegue"
    }

}

extension GamesListViewController: UITableViewDelegate {

}

extension GamesListViewController: UITableViewDataSource {
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return games.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: GameTableViewCell? = tableView.dequeueReusableCellWithIdentifier(Constants.cellId) as? GameTableViewCell

        if (cell == nil) {
            cell = GameTableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: Constants.cellId)
        }
        cell?.initWithGame(games[indexPath.row])

        return cell!
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.Delete) {

            let game = games[indexPath.row]
            self.games.removeAtIndex(indexPath.row)

            let realm = try! Realm()
            try! realm.write {
                realm.delete(game)
            }

            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Left)

        }
    }


}