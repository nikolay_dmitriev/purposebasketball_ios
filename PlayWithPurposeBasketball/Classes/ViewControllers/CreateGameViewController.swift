import Foundation
import UIKit
import Fusuma
import RealmSwift
import MBProgressHUD

class CreateGameViewController: UIViewController, UITextFieldDelegate {

//Mark:-Outlets

    @IBOutlet weak var avatarImageView: UIImageView!

    @IBOutlet weak var playerNameTextField: UITextField!
    @IBOutlet weak var playerNumberTextField: UITextField!
    @IBOutlet weak var tournamentTextField: UITextField!

//Mark:-Variables

    var game: GameModel?;

//Mark:-Public

    func initWithGame(game: GameModel?) {
        self.game = game;
    }

//Mark:-Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        tryToInitFieldsWithGame()

        playerNameTextField.delegate = self
        playerNumberTextField.delegate = self
        tournamentTextField.delegate = self
    }

    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        registerKeyboardNotifications()
    }

    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        deregisterKeyboardNotifications()
    }

//Mark:-Actions

    @IBAction func doneButtonTapped(sender: AnyObject) {
        let progressIndicator = MBProgressHUD.showHUDAddedTo(view, animated: true)
        progressIndicator.labelText = "Saving"
        progressIndicator.userInteractionEnabled = false
        UIApplication.sharedApplication().beginIgnoringInteractionEvents()


        let gameId = game?.id

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            [unowned self] in
            if let realGameId = gameId {
                let realm = try! Realm()
                let actualThreadGame = realm.objects(GameModel.self).filter({ $0.id == realGameId }).first!

                try! realm.write {

                    actualThreadGame.avatarImageData = UIImagePNGRepresentation(self.imageWithImage(self.avatarImageView.image!, scaledToSize: CGSize(width: 256, height: 256)))
                    actualThreadGame.name = self.playerNameTextField.text ?? ""
                    actualThreadGame.playerNumber = self.playerNumberTextField.text ?? ""
                    actualThreadGame.tournamentName = self.tournamentTextField.text ?? ""
                }
            } else {
                let newGame = GameModel()

                //ToDo: Save Image here
                newGame.avatarImageData = UIImagePNGRepresentation(self.imageWithImage(self.avatarImageView.image!, scaledToSize: CGSize(width: 256, height: 256)))
                newGame.id = NSUUID().UUIDString
                newGame.name = self.playerNameTextField.text ?? ""
                newGame.playerNumber = self.playerNumberTextField.text ?? ""
                newGame.tournamentName = self.tournamentTextField.text ?? ""

                let realm = try! Realm()

                try! realm.write {
                    realm.add(newGame)
                }
            }

            dispatch_async(dispatch_get_main_queue()) {
                UIApplication.sharedApplication().endIgnoringInteractionEvents()
                progressIndicator.hide(true)
                self.navigationController?.popViewControllerAnimated(true)
            }
        }

    }

    @IBAction func changePictureTapped(sender: AnyObject) {
        let fusuma = FusumaViewController()
        fusuma.delegate = self
        fusuma.hasVideo = false // If you want to let the users allow to use video.
        self.presentViewController(fusuma, animated: true, completion: nil)
    }


//Mark:-TextFieldDelegate

    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        resetPosition()
        return true
    }

//Mark:-Keyboard

    func keyboardWasShown(notification: NSNotification) {
        if let keyboardRect = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            let visibleRect = tournamentTextField.frame;

            let visibleViewBottom = visibleRect.origin.y + visibleRect.size.height;
            let animationHeight = keyboardRect.origin.y - visibleViewBottom

            let isKeyboardUnderTextField = animationHeight >= 0;
            if (!isKeyboardUnderTextField) {
                return;
            }

            UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: {
                self.view.transform = CGAffineTransformMakeTranslation(0, -animationHeight / 2)
            }, completion: nil)
        }

    }

//Mark:-Private

    func registerKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().addObserver(self,
                selector: #selector(keyboardWasShown),
        name: UIKeyboardDidShowNotification,
        object: nil)
    }

    func deregisterKeyboardNotifications() {
        NSNotificationCenter.defaultCenter().removeObserver(self,
                name: UIKeyboardDidShowNotification,
                object: nil)
    }

    func resetPosition() {
        UIView.animateWithDuration(0.4, delay: 0, options: UIViewAnimationOptions.BeginFromCurrentState, animations: {
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
        }, completion: nil)
    }

    func tryToInitFieldsWithGame() {
        if let editGame = game {

            navigationItem.title = "Edit Game"

            avatarImageView.image = UIImage(data: editGame.avatarImageData!)
            playerNameTextField.text = editGame.name
            playerNumberTextField.text = editGame.playerNumber
            tournamentTextField.text = editGame.tournamentName
        } else {
            navigationItem.title = "Create Game"
        }
    }

    func imageWithImage(image:UIImage, scaledToSize newSize:CGSize) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage
    }

}

extension CreateGameViewController: FusumaDelegate {
    func fusumaImageSelected(image: UIImage) {
        self.avatarImageView.image = image;
    }

    func fusumaVideoCompleted(withFileURL fileURL: NSURL) {
    }

    func fusumaCameraRollUnauthorized() {
        //ToDO: Show Alert Here
    }
}

