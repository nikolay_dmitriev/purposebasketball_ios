//
// Created by Nikolay Dmitriev on 7/7/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class LogoViewController: UIViewController {

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            sleep(2)
            dispatch_async(dispatch_get_main_queue()) {
                [unowned self] in
                self.performSegueWithIdentifier(Constants.mainSegue, sender: self)
            }
        }

    }

    struct Constants {
        static let mainSegue = "startApplicationSegue"
    }

}
