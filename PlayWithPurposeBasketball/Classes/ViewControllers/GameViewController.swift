//
// Created by Nikolay Dmitriev on 7/3/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class GameViewController: UIViewController {

//Mark:Outlets

    @IBOutlet var labelsWithHelp: [LabelWithHelpText]!

    @IBOutlet weak var twoPointsMakeLabel: LabelWithCircle!
    @IBOutlet weak var twoPointsMissLabel: LabelWithCircle!
    @IBOutlet weak var twoPointsPtsLabel: UILabel!
    @IBOutlet weak var twoPointsPercentageLabel: UILabel!


    @IBOutlet weak var threePointsMakeLabel: LabelWithCircle!
    @IBOutlet weak var threePointsMissLabel: LabelWithCircle!
    @IBOutlet weak var threePtsLabel: UILabel!
    @IBOutlet weak var threePointsPercentageLabel: UILabel!


    @IBOutlet weak var freeThrowMakeLabel: LabelWithCircle!
    @IBOutlet weak var freeThrowMissLabel: LabelWithCircle!
    @IBOutlet weak var freeThrowPtsLabel: UILabel!
    @IBOutlet weak var freeThrowPercentageLabel: UILabel!

    @IBOutlet weak var assistsLabel: LabelWithCircle!
    @IBOutlet weak var stealsLabel: LabelWithCircle!
    @IBOutlet weak var blocksLabel: LabelWithCircle!
    @IBOutlet weak var orebsLabel: LabelWithCircle!
    @IBOutlet weak var drebsLabel: LabelWithCircle!


    @IBOutlet weak var rewardImageView: UIImageView!
    @IBOutlet weak var rewardLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var timerView: TimerView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var timerBackgroundVIew: UIView!

//Mark:-Variables

    var currentGame: GameModel?
    var helpTextToShow = ""

//Mark:-Lifecycle

    override func viewDidLoad() {

        super.viewDidLoad()

        for label in labelsWithHelp {
            label.initWithTapListener({
                [unowned self] (helpText) in
                self.helpTextToShow = helpText
                self.performSegueWithIdentifier(Constants.helpSegue, sender: self)
            })
        }

        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTimerTextLabelTap(_:)))
        timerLabel.addGestureRecognizer(tapGesture)
        timerLabel.userInteractionEnabled = true

        initUi()
        updateUi()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)

        if segue.identifier == Constants.helpSegue {
            ((segue.destinationViewController) as! InformationViewController).informationText = helpTextToShow
        }
    }

//Mark:-Actions

    @IBAction func onHelpTap(sender: AnyObject) {
        self.helpTextToShow = "The red timer circle button can be used to start and stop the clock to keep track of how much time your player is in the game.The circle buttons for all of the stats can be pressed to add one, press and hold the button to subtract one.The stats for the game can be shared by pressing the share button in upper right corner.Press and hold the button label name for a definition of each one."
        self.performSegueWithIdentifier(Constants.helpSegue, sender: self)
    }


    @IBAction func onShareTap(sender: AnyObject) {
        shareResult()
    }

    func onTimerTextLabelTap(sender: UITapGestureRecognizer) {
        timerView.stop()
        let storyboard = UIStoryboard(name: "TimePickerStoryboard", bundle: nil)
        let controller: TimePickerViewController = storyboard.instantiateInitialViewController() as! TimePickerViewController
        controller.modalPresentationStyle = UIModalPresentationStyle.OverFullScreen
        controller.totalTime = currentGame!.gameLengthInSeconds

        controller.callback = ({
            [unowned self]
            seconds in
            let realm = try! Realm()
            try! realm.write {
                self.currentGame!.gameLengthInSeconds = seconds
            }
            self.timerView.updateUi()
        })

        presentViewController(controller, animated: false, completion: nil)
    }

    struct Constants {
        static let helpSegue = "showHelpGameSegue"
    }

//Mark:-Private

    private func updateUi() {
        if let game = currentGame {
            twoPointsMakeLabel.text = "\(game.twoPointsMake)"
            twoPointsMissLabel.text = "\(game.twoPointsMiss)"
            twoPointsPtsLabel.text = "\(game.twoPointsMake * 2)"
            twoPointsPercentageLabel.text = "\(game.percentage(game.twoPointsMake, miss: game.twoPointsMiss))"

            threePointsMakeLabel.text = "\(game.threePointsMake)"
            threePointsMissLabel.text = "\(game.threePointsMiss)"
            threePtsLabel.text = "\(game.threePointsMake * 2)"
            threePointsPercentageLabel.text = "\(game.percentage(game.threePointsMake, miss: game.threePointsMiss))"

            freeThrowMakeLabel.text = "\(game.freeThrowsMake)"
            freeThrowMissLabel.text = "\(game.freeThrowsMiss)"
            freeThrowPtsLabel.text = "\(game.freeThrowsMake)"
            freeThrowPercentageLabel.text = "\(game.percentage(game.freeThrowsMake, miss: game.freeThrowsMiss))"

            assistsLabel.text = "\(game.assists)"
            stealsLabel.text = "\(game.steals)"
            blocksLabel.text = "\(game.blocks)"
            orebsLabel.text = "\(game.orebs)"
            drebsLabel.text = "\(game.drebs)"

            pointsLabel.text = "Points: \(game.points)"

            if game.getReward().0 {
                rewardImageView.alpha = 1
                rewardLabel.alpha = 1
                rewardLabel.text = game.getReward().1
            } else {
                rewardImageView.alpha = 0
                rewardLabel.alpha = 0
            }
        }
    }

    private func shareResult() {
        guard let game = currentGame else {
            return
        }

        var textToShare = game.title
        textToShare += "\n\(game.formattedDate)"
        textToShare += "\nPWP Points: \(game.pwp)"
        textToShare += "\nPoints: \(game.points), Assists: \(game.assists), Steals: \(game.steals), Blocks: \(game.blocks), Rebounds: \(game.rebounds)"

        let shareItems = [textToShare]

        let activityController = UIActivityViewController(activityItems: shareItems, applicationActivities: nil)
        presentViewController(activityController, animated: false, completion: nil)
    }

    private func initUi() {
        guard let game = currentGame else{
            return
        }

        avatarImageView.image = UIImage(data: game.avatarImageData!)
        timerView.initWith(timerLabel, timerBackgroundView: timerBackgroundVIew, game: game)
        titleLabel.text = game.title

        twoPointsMakeLabel.initWithTapListener {
            [unowned self] in
            game.twoPointsMake = self.keepInRange(game.twoPointsMake + 1)
            self.updateUi()
        }
        twoPointsMakeLabel.initWithLongTapListener {
            [unowned self] in
            game.twoPointsMake = self.keepInRange(game.twoPointsMake - 1)
            self.updateUi()
        }

        twoPointsMissLabel.initWithTapListener {
            [unowned self] in
            game.twoPointsMiss = self.keepInRange(game.twoPointsMiss + 1)
            self.updateUi()
        }
        twoPointsMissLabel.initWithLongTapListener {
            [unowned self] in
            game.twoPointsMiss = self.keepInRange(game.twoPointsMiss - 1)
            self.updateUi()
        }

        //

        threePointsMakeLabel.initWithTapListener {
            [unowned self] in
            game.threePointsMake = self.keepInRange(game.threePointsMake + 1)
            self.updateUi()
        }
        threePointsMakeLabel.initWithLongTapListener {
            [unowned self] in
            game.threePointsMake = self.keepInRange(game.threePointsMake - 1)
            self.updateUi()
        }

        threePointsMissLabel.initWithTapListener {
            [unowned self] in
            game.threePointsMiss = self.keepInRange(game.threePointsMiss + 1)
            self.updateUi()
        }
        threePointsMissLabel.initWithLongTapListener {
            [unowned self] in
            game.threePointsMiss = self.keepInRange(game.threePointsMiss - 1)
            self.updateUi()
        }

        //

        freeThrowMakeLabel.initWithTapListener {
            [unowned self] in
            game.freeThrowsMake = self.keepInRange(game.freeThrowsMake + 1)
            self.updateUi()
        }
        freeThrowMakeLabel.initWithLongTapListener {
            [unowned self] in
            game.freeThrowsMake = self.keepInRange(game.freeThrowsMake - 1)
            self.updateUi()
        }

        freeThrowMissLabel.initWithTapListener {
            [unowned self] in
            game.freeThrowsMiss = self.keepInRange(game.freeThrowsMiss + 1)
            self.updateUi()
        }
        freeThrowMissLabel.initWithLongTapListener {
            [unowned self] in
            game.freeThrowsMiss = self.keepInRange(game.freeThrowsMiss - 1)
            self.updateUi()
        }

        //

        assistsLabel.initWithTapListener {
            [unowned self] in
            game.assists = self.keepInRange(game.assists + 1)
            self.updateUi()
        }
        assistsLabel.initWithLongTapListener {
            [unowned self] in
            game.assists = self.keepInRange(game.assists - 1)
            self.updateUi()
        }

        blocksLabel.initWithTapListener {
            [unowned self] in
            game.blocks = self.keepInRange(game.blocks + 1)
            self.updateUi()
        }
        blocksLabel.initWithLongTapListener {
            [unowned self] in
            game.blocks = self.keepInRange(game.blocks - 1)
            self.updateUi()
        }

        stealsLabel.initWithTapListener {
            [unowned self] in
            game.steals = self.keepInRange(game.steals + 1)
            self.updateUi()
        }
        stealsLabel.initWithLongTapListener {
            [unowned self] in
            game.steals = self.keepInRange(game.steals - 1)
            self.updateUi()
        }

        drebsLabel.initWithTapListener {
            [unowned self] in
            game.drebs = self.keepInRange(game.drebs + 1)
            self.updateUi()
        }
        drebsLabel.initWithLongTapListener {
            [unowned self] in
            game.drebs = self.keepInRange(game.drebs - 1)
            self.updateUi()
        }

        orebsLabel.initWithTapListener {
            [unowned self] in
            game.orebs = self.keepInRange(game.orebs + 1)
            self.updateUi()
        }
        orebsLabel.initWithLongTapListener {
            [unowned self] in
            game.orebs = self.keepInRange(game.orebs - 1)
            self.updateUi()
        }

    }

    private func keepInRange(value: Int) -> Int {
        if value < 0 {
            return 0
        }
        if value > 99 {
            return 99
        }
        return value
    }


}
