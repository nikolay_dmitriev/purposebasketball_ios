//
// Created by mac-184 on 7/6/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift


@IBDesignable
class LabelWithCircle: UILabel {

//Mark:-Private

    @IBInspectable
    var borderColor: UIColor = UIColor.greenColor(){
        didSet{
            updateWithBorder()
        }
    }

    @IBInspectable
    var borderWidth: Int = 1{
        didSet{
            updateWithBorder()
        }
    }

    private var singleTapCallback: (() -> ())?
    private var longTapCallback: (() -> ())?

//Mark:-Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.userInteractionEnabled = true
        updateWithBorder()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        updateWithBorder()
    }


//Mark:-Private

    func updateWithBorder() {
        layer.borderColor = borderColor.CGColor
        layer.borderWidth = 2
        layer.cornerRadius = bounds.width / 2
        userInteractionEnabled = true
    }

    func initWithTapListener(functionToCallOnTap: () -> ()) {
        singleTapCallback = functionToCallOnTap

         let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap(_:)))
        addGestureRecognizer(tapGesture)
    }

    func initWithLongTapListener(functionToCallOnTap: () -> ()) {
        longTapCallback = functionToCallOnTap

        let tapGesture = UILongPressGestureRecognizer(target: self, action: #selector(onLongTap(_:)))
        tapGesture.minimumPressDuration = 1.0
        addGestureRecognizer(tapGesture)
    }

    func onTap(sender: UITapGestureRecognizer) {
        let realm = try! Realm()
        try! realm.write {
            singleTapCallback?()
        }
    }

    func onLongTap(sender: UITapGestureRecognizer) {
        if sender.state != UIGestureRecognizerState.Began {
            return
        }
        let realm = try! Realm()
        try! realm.write {
            longTapCallback?()
        }
    }

}
