//
// Created by mac-184 on 7/6/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class LabelWithHelpText: UILabel {

//Mark:-Private

    @IBInspectable
    var helpText: String = ""
    private var singleTapCallback: ((helpText:String) -> ())?

//Mark:-Init

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.userInteractionEnabled = true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.userInteractionEnabled = true
    }

//Mark:-Public

    func initWithTapListener(functionToCallOnTap: (helpText:String) -> ()) {
        singleTapCallback = functionToCallOnTap

        let tapGesture = UILongPressGestureRecognizer(target: self, action: #selector(LabelWithHelpText.tapHandler(_:)))
        tapGesture.minimumPressDuration = 1
        addGestureRecognizer(tapGesture)
    }

    func tapHandler(sender: UITapGestureRecognizer) {
        if sender.state == UIGestureRecognizerState.Began {

            singleTapCallback?(helpText: helpText)
        }
    }

}
