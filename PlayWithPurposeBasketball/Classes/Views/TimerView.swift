import Foundation
import UIKit
import RealmSwift

@IBDesignable
class TimerView: UIImageView {

//Mark:-Variables

    @IBInspectable
    var activeImage: UIImage = UIImage()

    @IBInspectable
    var inactiveImage: UIImage = UIImage() {
        didSet {
            self.image = inactiveImage
        }
    }

    private weak var textLabel: UILabel?
    private weak var timerBackgroundView: UIView!
    private var game: GameModel?

    private var timer: NSTimer?

//Mark:-Lifestyle

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.userInteractionEnabled = true
        addTapListener()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.userInteractionEnabled = true
        addTapListener()
    }

    func initWith(label: UILabel, timerBackgroundView: UIView, game: GameModel) {
        textLabel = label
        self.game = game
        self.timerBackgroundView = timerBackgroundView

        timerBackgroundView.layer.cornerRadius = timerBackgroundView.frame.size.width / 2
        timerBackgroundView.backgroundColor = UIColor.redColor()

        textLabel?.text = "\(game.gameLengthInSeconds)"
        updateUi()
    }

//Mark:-Private

    private func addTapListener() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onTap(_:)))
        addGestureRecognizer(tapGesture)
    }

    func onTap(sender: UITapGestureRecognizer) {
        toggle()
    }

    func updateCounter() {
        guard let actualGame = game else{
            return
        }

        if actualGame.gameLengthInSeconds >= 60 * 30 {
            return
        }

        let realm = try! Realm()
        try! realm.write {
            actualGame.gameLengthInSeconds += 1
        }
        updateUi()
    }

    func updateUi() {
        guard let actualGame = game else{
            return
        }

        let minutes = actualGame.gameLengthInSeconds / 60
        let seconds = actualGame.gameLengthInSeconds - minutes * 60

        textLabel?.text = String(format: "Time: %02d:%02d", minutes, seconds)
    }

}

//Mark:-Timer Related

extension TimerView {

    func start() {
        self.image = activeImage
        textLabel?.textColor = UIColor.greenColor()
        timerBackgroundView.backgroundColor = UIColor.greenColor()
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(TimerView.updateCounter), userInfo: nil, repeats: true)
    }

    func stop() {
        self.image = inactiveImage
        textLabel?.textColor = UIColor.whiteColor()
        timerBackgroundView.backgroundColor = UIColor.redColor()
        
        if let existingTimer = timer {
            existingTimer.invalidate()
            timer = nil
        }
    }

    private func toggle() {
        timer == nil ? start() : stop()
    }

}
