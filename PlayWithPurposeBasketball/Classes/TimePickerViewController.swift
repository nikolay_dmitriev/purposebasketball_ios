//
//  TimePickerViewController.swift
//  TestStackView
//
//  Created by mac-184 on 8/8/16.
//  Copyright © 2016 NikolayDmitriev. All rights reserved.
//

import UIKit

class TimePickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

//Mark:-Outlets

    @IBOutlet weak var picker: UIPickerView!

//Mark:-Variables

    var callback:((totalSeconds:Int)->())?
    var totalTime: Int = 125;
    
    private var model = TimePickerViewModel()

//Mark:-Lifecycle

    override func viewDidLoad() {
        picker.dataSource = self
        picker.delegate = self
        setTime(totalTime)
    }

//Mark:-Actions

    @IBAction func onCancelTap(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func onOkTap(sender: AnyObject) {
        dismissViewControllerAnimated(true, completion: nil)
        if let resultCallback = callback{
            resultCallback(totalSeconds: getTime());
        }
    }

//Mark:-UIPickerDelegate, UIPickerDataSource

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return model.numberOfComponents
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return model.numberOfRowsInComponent(component)
    }

    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return "\(row)";
    }

//Mark:-Private

    private func setTime(totalSeconds: Int) {
        let minutes = totalSeconds / 60;
        let seconds = totalSeconds - minutes * 60;
        picker.selectRow(minutes, inComponent: 0, animated: false)
        picker.selectRow(seconds, inComponent: 1, animated: false)
    }

    private func getTime() -> Int {
        let minutes = picker.selectedRowInComponent(0)
        let seconds = picker.selectedRowInComponent(1)
        return minutes * 60 + seconds
    }

}

class TimePickerViewModel {

//Mark:-Variables

    let numberOfComponents = 2;

//Mark:-Public

    func numberOfRowsInComponent(component: Int) -> Int {
        return component == 0 ? Constants.minutes : Constants.seconds
    }

//Mark:-Constants

    private struct Constants {
        static let minutes = 31
        static let seconds = 61
    };
}