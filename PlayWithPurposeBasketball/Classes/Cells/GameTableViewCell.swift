//
// Created by mac-184 on 7/6/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import UIKit

class GameTableViewCell: UITableViewCell {


//Mark:-Outlets

    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var rewardBallImageView: UIImageView!

    @IBOutlet weak var dateLabel: UILabel!

    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var assistsLabel: UILabel!
    @IBOutlet weak var stealsLabel: UILabel!
    @IBOutlet weak var blocksLabel: UILabel!
    @IBOutlet weak var rebounds: UILabel!
    @IBOutlet weak var pwpPointsLabel: UILabel!

//Mark:-Public

    func initWithGame(game: GameModel) {
        nameLabel.text = game.title
        dateLabel.text = game.formattedDate

        pointsLabel.text = "Points\n\(game.points)"
        assistsLabel.text = "Assists\n\(game.assists)"
        stealsLabel.text = "Steals\n\(game.steals)"
        blocksLabel.text = "Blocks\n\(game.blocks)"
        rebounds.text = "Rebounds\n\(game.rebounds)"
        pwpPointsLabel.text = "PWP Points: \(game.pwp)"

        avatarImageView.image = UIImage(data: game.avatarImageData!)

        rewardBallImageView.alpha = game.getReward().0 ? 1 : 0
    }

}
