//
// Created by Nikolay Dmitriev on 6/30/16.
// Copyright (c) 2016 NikolayDmitriev. All rights reserved.
//

import Foundation
import RealmSwift;
import UIKit

class GameModel: Object {

//Mark:-Variables

    dynamic var id = ""

    override static func primaryKey() -> String? {
        return "id"
    }

    dynamic var name = ""
    dynamic var avatarImageData: NSData?
    dynamic var date = NSDate()
    dynamic var playerNumber = ""
    dynamic var tournamentName = ""
    dynamic var gameLengthInSeconds = 0

    dynamic var twoPointsMake = 0
    dynamic var twoPointsMiss = 0
    dynamic var threePointsMake = 0
    dynamic var threePointsMiss = 0
    dynamic var freeThrowsMake = 0
    dynamic var freeThrowsMiss = 0

    dynamic var assists = 0
    dynamic var blocks = 0
    dynamic var steals = 0
    dynamic var orebs = 0
    dynamic var drebs = 0

}

extension GameModel {

    //Mark:-Compted Properties

    var points: Int {
        return twoPointsMake * 2 + threePointsMake * 3 + freeThrowsMake;
    }

    var rebounds: Int {
        return orebs + drebs;
    }

    var pwp: Int {
        return points + (rebounds + assists + blocks + steals) * 2;
    }

    var formattedDate: String {
        let dayTimePeriodFormatter = NSDateFormatter()
        dayTimePeriodFormatter.dateFormat = "MMMM dd, yyyy"
        return dayTimePeriodFormatter.stringFromDate(date)
    }

    var title: String {
        var result = name

        result += playerNumber == "" ? "" : ", #" + "\(playerNumber)"
        result += tournamentName == "" ? "" : ", " + "\(tournamentName)"

        return result
    }

    func percentage(make: Int, miss: Int) -> Int {
        if miss == 0 {
            return 100
        }
        if make == 0 {
            return 0
        }
        return ((make * 100) / (make + miss));

    }

    func getReward() -> (Bool, String) {
        var result = 0

        result += points >= 10 ? 1 : 0;
        result += assists >= 10 ? 1 : 0;
        result += blocks >= 10 ? 1 : 0;
        result += steals >= 10 ? 1 : 0;
        result += orebs + drebs >= 10 ? 1 : 0;

        return (result >= 2, getStringRewardName(result))
    }

    private func getStringRewardName(result: Int) -> String {
        switch result {
        case 2: return "Double-Double"
        case 3: return "Triple-Double"
        case 4: return "Quadruple-Double"
        case 5: return "Quintuple-Double"
        default: return ""
        }

    }

}